package library;

public class Main {

    public static void main(String[] args){
//Utwórz klasę Książka o takich polach jak nazwa, autor, rok wydania.
//Dodaj konstruktor, aby łatwiej tworzyć nowe książki oraz nadpisz .toString(), aby wyświetlić czytelny opis książki
// jak np. „Krzyżacy”, Henryk Sienkiewicz 2004.
//Dodaj klasę Biblioteka, która będzie przechowywać książki i umożliwiać ich wypożyczanie. Utwórz takie metody jak:
//        a. czyKsiazkaDostepna(String nazwa Ksiazki):boolean
//        b. wypozyczKsiazke(String nazwa Ksiazki):Ksiazka
//        c. wyswietlWypozyczoneKsiazki():void
//        d. wyswietlDostepneKsiazki():void
//        e. Przetestuj rozwiązanie w psvm!

        Book book = new Book("Krzyżacy", "Henryk Sienkiewicz", 1991);
        Book book2 = new Book("Cos na probe", "Andrzej Sieczyk", 1995);
        Book book3 = new Book("Inna proba", "Nowak Zdzichu", 2009);
        Book book4 = new Book("Jak byc w formie po 40", "Krzysztof Ibisz", 2017);

        Library library = new Library();
        library.showAllBooks(book);
        library.showAllBooks(book2);
        library.showAllBooks(book3);
        library.showAllBooks(book4);

    }
}
