package library;

public class Book {
//polach jak nazwa, autor, rok wydania. Dodaj konstruktor, aby łatwiej tworzyć nowe książki
    private String nameOfBook;
    private String authorOfBook;
    private int yearOfBookEdition;

    public Book(String nameOfBook, String authorOfBook, int yearOfBookEdition) {
    this.nameOfBook = nameOfBook;
    this.authorOfBook = authorOfBook;
    this.yearOfBookEdition = yearOfBookEdition;
   }
    @Override
    public String toString() {
        return "Book{" +
                "nameOfBook=' " + nameOfBook + '\'' +
                ", authorOfBook=' " + authorOfBook + '\'' +
                ", yearOfBookEdition= " + yearOfBookEdition +
                '}';
    }

    public String getNameOfBook() {
        return nameOfBook;
    }

    public String getAuthorOfBook() {
        return authorOfBook;
    }

    public int getYearOfBookEdition() {
        return yearOfBookEdition;
    }



}
